import numpy as np
import utils

class GameState():
    def __init__(self, state, parent = None, eval_func = None):
        level = parent.get_level() + 1 if parent else 0

        self.__state  = state
        self.__level  = level
        self.__score  = 0
        self.__parent = parent

        if eval_func:
            self.__score = eval_func(self)

    def __hash__(self):
        return hash(str(self.__state))

    def __lt__(self, other):
        return self.__score < other.__score

    def __eq__(self, other):
        return self.__score == other.__score

    def __gt__(self, other):
        return self.__score > other.__score

    def get_state(self):
        return self.__state

    def get_score(self):
        return self.__score

    def get_level(self):
        return self.__level

    def get_parent(self):
        return self.__parent

def manhattan_distance(cur, goal):
    cur_state  = cur.get_state()
    goal_state = goal
    n = int(np.sqrt(len(cur_state))) # XXX

    score = cur.get_level()

    for cur_tile in cur_state:
        cur_idx  = cur_state.index(cur_tile)
        goal_idx = goal_state.index(cur_tile)
        cur_i, cur_j   = utils.idx(cur_idx, n)
        goal_i, goal_j = utils.idx(goal_idx, n)
        score += abs(cur_i - goal_i) + abs(cur_j - goal_j)

    return score

def greedy(cur, goal):
    cur_state  = cur.get_state()
    goal_state = goal
    n = int(np.sqrt(len(cur_state))) # XXX

    score = 0

    for cur_tile in cur_state:
        cur_idx  = cur_state.index(cur_tile)
        goal_idx = goal_state.index(cur_tile)
        cur_i, cur_j   = utils.idx(cur_idx, n)
        goal_i, goal_j = utils.idx(goal_idx, n)
        score += abs(cur_i - goal_i) + abs(cur_j - goal_j)

    return score
