from queue import PriorityQueue, Queue, LifoQueue
from game_state import GameState, manhattan_distance, greedy
import numpy as np
import time
import utils

class Solver():
    def __init__(self, init_state, goal_state, algo, eval_func, max_iter = 2500):
        self.__init_state = init_state.flatten().tolist()
        self.__goal_state = goal_state.flatten().tolist()
        self.__dim = goal_state.shape[0]
        self.__MAX = 100000
        self.__max_iter = max_iter
        self.__path = []
        self.__number_of_steps = 0
        self.__summary = ""

        self.__algo      = algo
        self.__eval_func = eval_func
        
    def set_max_iter(self, max_iter):
        self.__max_iter = max_iter
        
    def get_path(self):
        return self.__path
    
    def get_summary(self):
        return self.__summary

    def set_summary(self, level, count, time):
        self.__summary  = "{} took {} steps to get from initial state to the desired goal, ".format(self.__eval_func, level)
        self.__summary += "visited total of {} nodes, and took around {} seconds to reach the desired solution.".format(count, time)

    def eval_func(self, state):
        if   self.__eval_func == "no_heuristic":
            return 0
        elif self.__eval_func == "manhattan":
            return manhattan_distance(state, self.__goal_state)
        elif self.__eval_func == "greedy":
            return greedy(state, self.__goal_state)
        else:
            raise RuntimeError("Unkown evaluation function")

    def build_path(self, node):
        while node.get_parent():
            self.__path.append(node)
            node = node.get_parent()
        
    def solve(self):
        possible_moves = list(zip([1, 0, -1,  0], [0, 1,  0, -1]))
        
        visited_nodes = set()
        
        start_time = time.clock()
		
        if self.__algo == ("A_star" or "UCS" or "GBFS") :
            nodes = PriorityQueue(self.__MAX)
        elif self.__algo == "DFS":
            nodes = LifoQueue(self.__MAX)
        else:
            nodes = Queue(self.__MAX)
        
        # Setup initial node
        init_node = GameState(self.__init_state, None, self.eval_func)

        # Add initial node to the queue
        nodes.put(init_node)
        
        epochs = 0
        while nodes.qsize() and epochs <= self.__max_iter:
            epochs += 1
            
            # Pop node from the queue
            cur_node  = nodes.get()
            cur_state = cur_node.get_state()
            
            # Skip if node is already visited -- TODO replace if cost is lower
            if str(cur_state) in visited_nodes:
                continue

            # Add current node to the set of visited nodes
            visited_nodes.add(str(cur_state))
            
            if cur_state == self.__goal_state:
                self.set_summary(cur_node.get_level(), epochs, np.round(time.clock() - start_time, 4))
                self.build_path(cur_node)
                break
            
            # Get dimension of the grid
            dim = self.__dim

            # Get empty tile position
            empty_tile = cur_state.index(0)
            i, j = utils.idx(empty_tile, dim)
            
            # Build current state into dim x dim matrix
            cur_state = np.array(cur_state).reshape(dim, dim)

            # Check all possible states from current state
            for x, y in possible_moves:
                new_state = np.array(cur_state)
                if i + x >= 0 and i + x < dim and j + y >= 0 and j + y < dim:
                    # If state is possible (i.e. not outside the grid, build the new state array)
                    new_state[i, j], new_state[i+x, j+y] = new_state[i+x, j+y], new_state[i, j]

                    # Build GameState object from state array
                    game_state = GameState(new_state.flatten().tolist(), cur_node, self.eval_func)

                    # Add to frontier if not in visited nodes
                    if str(game_state.get_state()) not in visited_nodes:
                        nodes.put(game_state)

        if epochs > self.__max_iter:
            print('This grid setting is not solvable')

        return self.__path
